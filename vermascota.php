<?php

include("conexion.php");
session_start();
$rol=$_SESSION['cod_rol'];


?>

<!DOCTYPE HTML>
<html lang="en">

<head>
    <title> Mascotas</title>
 
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/
  fontello.css">
  <link rel="stylesheet" type="text/css" href="estilossss.css">


</head>
  
  
<body>
  <div class="encabezado">  
          <h1>Lista de Mascotas </h1>
  </div>

              <?php
    
 if($rol==1){               // si esta logiado vera esta informacion  
?>
<dir style="margin-left: 18%">
      <button    type="button" class="btn btn-success" onclick="window.location='mascota.php'">agregar mascosta</button>
</dir>

<?php
 }
    
?>

<div class="contenedor3">  

  <div class="table-responsive">          
    <table class="table table-striped table-hover" id="tabla">
      <thead class="thead-green">
        <tr>
        <th>codigo mascota</th>
        <th>Nombre </th>
       <th>Dueño de la mascota</th>
       <th>editar</th>
        
        
        </tr>
    </thead>
    <tbody class="tbody-green">
        <?php
      
          $consulta7 = mysqli_query ($con, "SELECT * FROM mascota");
            while($mostrar4=mysqli_fetch_array($consulta7)){
        ?>
      
          <?php
    
 if($rol==1){               // si esta logiado vera esta informacion  
?>  
        <tr>
                                    <td><?php echo $mostrar4['cod_mascota'] ?></td>
                                    <td><?php echo $mostrar4['nombre_m'] ?></td>
                                    <td><?php echo $mostrar4['rut_usuario'] ?></td>
                                   



                                    <td>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <a href="modificarmascota.php?cod_mascota=<?php echo $mostrar4['cod_mascota'] ?>" class="btn btn-info">Modificar</a>
                                            </div>
                                            <div class="col-md-3">
                                                <button type="submit" onclick="confirmar(<?php echo $mostrar4['cod_mascota'] ?>)" class="btn btn-danger">Eliminar</button>
                                            </div>
                                             <div class="col-md-3">
                                                <a href="verfichamedica.php?cod_mascota=<?php echo $mostrar4['cod_mascota'] ?>" class="btn  btn-secondary">Ficha medica</a>
                                            </div>
                                        </div>
                                    </td>
                    
                                </tr>
        

<?php
 }
    
?>

          <?php
    
 if($rol==2){               // si esta logiado vera esta informacion  
?>
<tr>

                                    <td><?php echo $mostrar4['cod_mascota'] ?></td>
                                    <td><?php echo $mostrar4['nombre_m'] ?></td>
                                    <td><?php echo $mostrar4['rut_usuario'] ?></td>
                                   



                                    <td>

                                        <div class="row">
                                            
                                            
                                             <div class="col-md-3">
                                                <a href="verfichamedica.php?cod_mascota=<?php echo $mostrar4['cod_mascota'] ?>" class="btn  btn-secondary">Ficha medica</a>
                                            </div>
                                        </div>
                                    </td>
                    
                                </tr>



<?php
 }
    
?>

        <?php }?>
    </tbody>
    </table>

  </div>


  </div>

</div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').DataTable({
                language: {
                    search: "Buscar:",
                    paginate: {
                        first: "Primer",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último"
                    },
                    info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                    emptyTable: "No existen elementos para mostrar en la tabla",
                    infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                    infoFiltered: "(Filtrado de _MAX_ resultados)",
                    lengthMenu: "Mostrando _MENU_ resultados",
                    loadingRecords: "Cargando...",
                    processing: "Procesando...",
                    zeroRecords: "No se encontraron resultados",
                    aria: {
                        sortAscending: ": Ordenado de forma ascendente",
                        sortDescending: ": Ordenado de forma descendente"
                    }

                }
            });
        });

    </script>

 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <script>
    function confirmar(cod_mascota){
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
        title: '¿Está seguro de eliminar?',
        text: "No podrá recuperar la infromacion!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, eliminar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
        }).then((result) => {
        if (result.value) {
            eliminar(cod_mascota)
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelado',
            'el tipo  no se eliminará',
            'error'
            )
        }
        })
    }

    function eliminar(cod_mascota) {
         cadena = "cod_mascota="+cod_mascota;
        console.log(cadena)

        $.ajax({
            type: "POST",
            url: "eliminarmascota.php",
            data: cadena,
            success: function(r) {
                if (r == 1) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Eliminación exitosa',
                        text: "Eliminado",

                        showCancelButton: false,
                        confirmButtonColor: '#0867F4',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Continuar'
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    })

                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'No se ha podido eliminar',
                        text: 'Ocurrió un error interno'
                    })
                }
            }
        })
    }
</script>
</body>
</html>