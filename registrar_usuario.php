<?php

    include("vista.php");

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/main.css" type="text/css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Registrar Usuario</title>
</head>
<body style="background-color: gray">
    <div class="contenedor">
    <div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div class="panel panel-danger">
					
					<div class="panel panel-body">
						<form id="frmRegistro" action="registro2.php" method="POST">
						<label><b>Registro de Usuario</b></label><br><br>
							<label>Nombre (requerido)</label>
							<input maxlength="20" minlength="2" type="text" class="form-control input-sm" name="nombre" id="nombre" required placeholder="Ingrese Nombre">
							<label>Apellido</label>
							<input maxlength="20" minlength="2" type="text" class="form-control input-sm" name="apellido" id="apellido" placeholder="Ingrese Apellido">
							<label>Rut (requerido)</label>
							<input maxlength="10"  type="text" class="form-control input-sm" name="usuario" id="usuario" required oninput="checkRut(this)" placeholder="Ingrese Rut">
							<label>Correo (requerido)</label>
							<input maxlength="40" type="email" class="form-control input-sm" name="correo" id="correo" required placeholder="Ingrese Correo">
							<label>Contraseña (requerido)</label>
							<input maxlength="20"  type="password" class="form-control input-sm" name="password" id="password" required placeholder="Ingrese Contraseña">
							<label>Confirmar Contraseña (requerido)</label>
							<input maxlength="20"  type="password" class="form-control input-sm" name="password2" id="password2" required placeholder="Confirme Contraseña">
							<label>Telefono</label>
							<input maxlength="9" type="tel" class="form-control input-sm" name="telefono" id="telefono" pattern="[0-9]{9}" placeholder="Ingrese Telefono">
							<label>Dirección</label>
							<input maxlength="40" type="text" class="form-control input-sm" name="direccion" id="direccion" placeholder="Ingrese Dirección">
							<p><br>
							<input type="submit" class="btn btn-success btn-primary btn-block" values="Aceptar" onclick="comprobarClave();" />
							<script src="validarRUT.js"></script><br>
							<a href="javascript: history.go(-1)" class="btn btn-warning btn-default btn-block" onclick="return confirm('Estás seguro que deseas volver?');">Volver</a>
							</p>
							
						</form>
					</div>
				</div>
			</div>
    </div>
    
</body>
</html>


<script>
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    
    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    
    // Formatear RUN
    rut.value = cuerpo + '-'+ dv
    
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}
    
    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;
    
    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++) {
    
        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        
        // Sumar al Contador General
        suma = suma + index;
        
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { rut.setCustomValidity("RUT Inválido"); return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}

</script>

<script>
	function comprobarClave(){
    if (frmRegistro.password.value != frmRegistro.password2.value){
	   		frmRegistro.password2.value="";
	   		frmRegistro.password.focus();
			   password2.setCustomValidity("contraseñas distintas"); 
			   return false;

	}
	else
	return true;
	
}

</script>

<script>
    function validaNumericos(){
 var inputtxt = document.getElementById('direccion'); 
 var valor = inputtxt.value;
 for(i=0;i<valor.length;i++){
     var code=valor.charCodeAt(i);
         if(code<=48 || code>=57){          
           inputtxt.value=""; 
           return;
         }    
   }
  
}
</script>