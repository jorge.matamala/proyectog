<?php
    include("conexion.php");
	session_start();
	
	$rut = $_SESSION['rut_usuario'];
	$rol = $_SESSION['cod_rol'];

    $consulta="SELECT * FROM usuario WHERE rut_usuario='$rut'";
    $resultado = mysqli_query($con, $consulta);  
    $row = mysqli_fetch_array($resultado);

    
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Perfil Usuario</title>
</head>
<body>
<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div class="panel panel-danger">
					
					<div class="panel panel-body">
						<form id="vistaregistro" action="modificar_usuario.php" method="POST">
						<br><label><b>Perfil de Usuario</b></label><br><br>
							<label>Nombre</label>
							<input maxlength="20" type="text" class="form-control input-sm"  name="nombre" id="nombre" value="<?php echo $row['nombre'] ?>" readonly>
							<label>Apellido</label>
							<input maxlength="20" type="text" class="form-control input-sm" name="apellido" id="apellido" value="<?php echo $row['apellido'] ?>" readonly>
							<label>Rut</label>
							<input maxlength="10"  type="text" class="form-control input-sm" name="rut" id="rut" value="<?php echo $row['rut_usuario'] ?>" readonly>
							<label>Correo</label>
							<input maxlength="40" type="email" class="form-control input-sm" name="correo" id="correo" value="<?php echo$row['correo'] ?>" readonly>
							<label>Telefono</label>
							<input maxlength="9" type="text" class="form-control input-sm" name="telefono" id="telefono" value="<?php  echo $row['telefono'] ?>" readonly>
							<label>Dirección</label>
							<input maxlength="40" type="text" class="form-control input-sm" name="direccion" id="direccion" value="<?php echo $row['direccion'] ?>" readonly>
							<p><br>
							<input type="submit" class="btn btn-success btn-primary btn-block" value="Modificar Perfil" />
							<a href="vermascota.php" class="btn btn-success btn-primary btn-block"  >Ver lista de mascota</a>
							</p>
							
						</form>
					</div>
				</div>
			</div>
</body>
</html>
